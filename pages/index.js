/**
 * @type {import('splendid').Page}
 */
// export
const index2 = {
 title: 'IOne',
 seo: 'Сервис будущего Кибер и Вам',
 og: {
  image: '/img/splash2.jpg',
 },
 links: {
  idealOne:'https://iDeal.One',
 },
}
// testing
export
const index = {
 file:'wide',
 title:'Кибер и Вам',
 // title: 'IOne',
 // title: 'IOne',
 seo: 'Сервис будущего Кибер и Вам',
 og: {
  // image: '/img/splash2.jpg',
 },
 links: {
  idealOne:'https://iDeal.One',
 },
}

// export const test={
//  title:'video-test',
// }
// additional directories
// export const dir = '~/dir'
