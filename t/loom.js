// let i
let s = ''
let mobs=''
let mobs_mb=''
let mobs_lg=''
let mobs2=''

const fix=(s)=>{
 return s.replace(/0+$/,'').replace(/\.$/,'')
}

for (let i=0; i<=100; i++) {
 const del=(i/16)
 const rem=fix(del.toFixed(3))

 const scoof=i<16?1.155:1.445
 const mcoof=i<16?1.135:1.425
 const lcoof=i<16?1.115:1.405

 const srem=fix((del/scoof).toFixed(3))
 const mrem=fix((del/mcoof).toFixed(3))
 const lrem=fix((del/lcoof).toFixed(3))
 s+=`
.FS${i}, .FontSize${i} { font-size: ${rem}rem; }`
 mobs+=`
  .FS${i}, .FontSize${i} { font-size: ${srem}rem; }`
 mobs_mb+=`
  .FS${i}, .FontSize${i} { font-size: ${mrem}rem; }`
 mobs_lg+=`
  .FS${i}, .FontSize${i} { font-size: ${lrem}rem; }`
 mobs2+=`
  .FSMB${i} { font-size: ${rem}rem; }`
}

for (let i=1; i<10; i++) {
 const j=i*100
 s+=`
.FW${j}, .FontWeight${j} { font-weight: ${j}; }`
}

// const weights=[100,200,300,400,500,600,700]

console.log(s.trim())

console.log('@media(max-width:576px) {')
console.log(' ',mobs.trim())
console.log('}')
console.log('@media(min-width:576px) and (max-width:768px) {')
console.log(' ',mobs_mb.trim())
console.log('}')
console.log('@media(min-width:768px) and (max-width:992px) {')
console.log(' ',mobs_lg.trim())
console.log('}')
console.log('@media(max-width:576px) {')
console.log(' ',mobs2.trim())
console.log('}')