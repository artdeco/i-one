import {readFileSync, writeFileSync} from 'fs'

const FILE='maurice/docs/index.html'
let f=readFileSync(FILE)+''

const DOMAIN='https://938fb211-da7f-44b3-a98c-92d4cfe7c616.selcdn.net'

// const DOMAIN='https://temporary-kettle.surge.sh'

f=f.replace(/(href|src|srcset|data-src-\d+)="(.+?)"/g,(m,$0,$1)=>{
 if($1.startsWith('data:')) return m
 if($1.startsWith('#')) return m
 if($1.startsWith('http:')||$1.startsWith('https:')) return m
 if(!$1.startsWith('/')) $1=`/${$1}`
 return `${$0}="${DOMAIN}${$1}"`
})
f=f.replace(/(,pages\/)/g,()=>{
 return `,${DOMAIN}/pages/`
})
f=f.replace(/\/fonts/g,()=>{
 return `${DOMAIN}/fonts`
})
let head='',body=''
f.replace(/<head>([\s\S]+?)<\/head>/,(m,$1)=>{
 head=$1
})
f.replace(/<body(.+?)>([\s\S]+?)<\/body>/,(m,attrs,$1)=>{
 body=`<div${attrs}>${$1}</div>`
})
writeFileSync(`tilda-head.html`,head)
writeFileSync(`tilda-body.html`,body)