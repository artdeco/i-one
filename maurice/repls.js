
export const REPLS=[]
REPLS.push({
 // todo: regex as jsx please
 re: /\s*(<!--\s*)?<(?:(?:<(.+?)>)|({.+?})|(\(.+?\)))?<([\w+.\d\-_~$:]+)([\s\S]+?)\/\s*>>(?:\s*-->)?/g,
 replacement(m,isComment,tagName,destr,args,partialName,partialContent,position,content) {
  // debugger
  if(isComment) return ''
  // m=m.replace(/<!--[\s\S]+?-->/g,'')
  try {
   // global partials?
   let acceptedArgs=[]
   if(args) {
    // debugger
    args=args.replace(/^\(|\)$/g,'')
    acceptedArgs=args.split(/\s*,\s*/)
   }
   //  debugger
   const updated=this.splendid.updateAreas(content)
   // if(!this.splendid.areas.length) {
   // debugger
   // console.log(content)
   // debugger
   // }
   const cf=updated===false?'UNDEFINED':this.splendid.getCurrentFile(position, this.splendid.areas)
   //  console.log(`〽️ Adding ${tagName?`tag ${tagName} as partial`:'partial'} %s ${
   //   tagName?'with':'as'
   //  } "%s" in %s`, partialName, partialContent.slice(0,25)+'.'.repeat(
   //   Math.max(0,Math.min(3,partialContent.length-25)),
   //  ),cf)
   if(!filePartialMap.has(cf)) {
    filePartialMap.set(cf, new Map)
   }
   const partialsMap=filePartialMap.get(cf)
   partialsMap.set(partialName,{
    content:partialContent,
    // attributes:partialContent,
    // args: args,
    // allArgs:allArgs,
    tagName:tagName,
    acceptsArgs:acceptedArgs.length>0,
    getContent(...argss) {
     if(!acceptedArgs.length) return partialContent
     if(!argss.length) return partialContent

     let p=partialContent
     const data=argss.reduce((acc,currentArg,i)=>{
      const argName=acceptedArgs[i]
      acc[argName]=currentArg
      return acc
     },{})
     for (const d in data) {
      const value=data[d]
      // p=.replace() todo - p=.replace
      p=p.replace(new RegExp(`(\\s\\S+?=)${d}`),(m,$1)=>{
       return `${$1}"${value}"`
      })
      // argument,value
     }
     return p
     // return
    },
   })
   return ''
  } catch (err) {
   console.warn(err)
   return m
  }
 },
}, {
 re: /(<[^>]+?)\s\.\.([\w+.\d-_$:]+(?:="(.+?)")?)(?:\s*>|\s[\s\S]+?>)/g,
 /**
  *
  * @param {string} m
  * @param {*} before
  * @param {*} partialName
  */
 replacement(m,before,firstPartialName,firstPartialArgs,position,content) {
  try {
   const updated=this.splendid.updateAreas(content)
   const cf=updated===false?'UNDEFINED':this.splendid.getCurrentFile(position, this.splendid.areas)
   if(!cf) {
    console.warn('could not detect current file')
    return m
   }
   const partialsMap=filePartialMap.get(cf)
   if(!partialsMap) {
    console.warn('📵 No partials for %s', cf)
    console.warn('Make sure the partial is available in the local file (globals not supported yet)')
    return''
   }
   const rm=replaceExpansions(m,partialsMap)
   // console.log(cf)
   // if(!partialsMap) {
   //   return m
   // }


   return rm
  } catch (err) {
   console.warn(err)
   return m
  }
 },
},{
 re:/(<\/?)(_[^\s>]+)/g,
 replacement: partialTagReplacement,
},{
 re:/(<\/?)([^\s>]+?~)/g,
 replacement: partialTagReplacement,
})

/**
 *
 * @param {string} m
 * @param {Map<string,?>} partialsMap
 */
function replaceExpansions(m,partialsMap) {
 let rm=m.replace(/\s\.\.([\w+.\d-_$:]+)(?:="(.+?)")?/g,(mm,partialName,partialArg)=>{
  // if(partialArg) debugger
  const partial=partialsMap.get(partialName)
  if(!partial) {
   console.warn('📵 No partial found for %s', partialName)
   return''
  }
  const{
   getContent:getContent,
  }=partial
  const c=partialArg?getContent(partialArg):getContent()
  // console.log('😎 Expanding "..%s" partial as "%s"', partialName, c.trim())
  return c
 })
 let howManyClassAttrs=0
 rm.replace(/\s+class="(.+?)"/g,(m,classes)=>{
  howManyClassAttrs++
 })
 if(howManyClassAttrs>1) {
  let allClasses=[]
  rm=rm.replace(/\s+class="(.+?)"/g,(m,classes)=>{
   allClasses.push(classes)
   return ''
  })
  rm=rm.replace(/>/,` class="${allClasses.join(' ')}">`)
 }
 return rm
}

function partialTagReplacement(m,b,partialName,position,content) {
 const closing=/\//.test(b)
 this.splendid.updateAreas(content)
 const cf=this.splendid.getCurrentFile(position,this.splendid.areas)
 if(!cf) {
  console.warn('could not detect current file')
  return m
 }
 const partialsMap=filePartialMap.get(cf)
 if(!partialsMap) {
  console.warn('📵 No partial found for %s in %s', partialName, cf)
  console.warn('Make sure the partial is available in the file (globals not supported yet)')
  return m
 }
 const partial=partialsMap.get(partialName)
 if(!partial) {
  console.warn('📵 No partial found for %s in %s', partialName, cf)
  return m
 }
 if(!partial.tagName) {
  console.warn('📵 No tag found for %s in %s', partialName, cf)
  return m
 }
 let cc=''
 if(!closing) {
  let partialContent=partial.content||''
  if(content) {
   content=replaceExpansions(partialContent,partialsMap)
  }
  cc=content.trim()
 }
 return `${b}${partial.tagName}${cc?` ${cc}`:cc}`
}

const filePartialMap=new Map
