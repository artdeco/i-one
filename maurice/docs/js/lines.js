function getD(item,duration=5500){
 var anime=item.getAnimations()
 if(!anime[0]) return 0
 var time=anime[0].currentTime
 // -anime[0].startTime
 var d=time%duration
 console.log('delay',d)
 return d
}
// 'FirstFrameAnimation'
function calc(ff,prop1,prop2,Class,duration) {
 var width=ff.getBoundingClientRect().width
 document.body.style.setProperty(prop1,width-1+'px')
 document.body.style.setProperty(prop2,width-1+'px')
 var d=getD(ff,duration)
 // var d=0 // getD(ff,7000)
 ff.style.removeProperty('animation-delay')
 ff.classList.remove(Class)
 setTimeout(function(){
  if(d) ff.style.setProperty('animation-delay',(-d)+'ms','important')
  ff.classList.add(Class)
 },1)
}
// debugger
function calc1() {
 /** @type {HTMLElement} */
 var ff=window['ff']
 calc(ff,'--ff-width','--ff-mob-width','FirstFrameAnimation',7000)
}
function calc2() {
 /** @type {HTMLElement} */
 var ff=window['ff2']
 calc(ff,'--ff2-width','--ff2-mob-width','FirstFrame2Animation',5500)
}
// window.addEventListener('resize',calc1)
window.addEventListener('load',calc1)
// window.addEventListener('resize',calc2)
window.addEventListener('load',calc2)