const CLASS = 'OnThisPage'

/**
 * @type {import('splendid').Element}
 */
export default function OnThisPage({ splendid, 'max-level': maxLevel = 2 }) {
  splendid.polyfill('intersection-observer')
  const cssPath = splendid.elementRelative('./on-this-page.css')
  splendid.css(cssPath, `.${CLASS}`, {
    whitelist: 'Active',
  })
  let last = null
  const headings = splendid.headings
    .reduce((acc, heading) => {
      const ret = () => {
        last = current
        return acc
      }
      const { level } = heading
      if (level > maxLevel) return acc

      const current = { heading, level, children: [] }
      if (!last) {
        acc.push(current)
        return ret()
      }
      if (last.level < level) {
        last.children.push(current)
        current.parent = last
        return ret()
        // stack.unshift(current)
      }
      let parent = last
      while ((parent = parent.parent)) {
        if (parent.level < level) {
          parent.children.push(current)
          current.parent = parent
          return ret()
        }
      }
      acc.push(current)
      return ret()
    }, [])
  // console.log(inspect(headings, { depth: 10 }))
  // process.exit(1)
  const hh = headings.map(({ heading: { title, id }, level, children }) => {
    title = splendid.clearTags(title)
    return (<Li key={id} id={id} title={title} inner={children} level={level}>
    </Li>)
  })
  return (<ul className={CLASS}>{hh}</ul>)
}

const Li = ({ id, title, inner = [], level }) => {
  // console.log(id, title, inner)
  return (<li data-heading={id}>
    <a href={`#${id}`} dangerouslySetInnerHTML={{ __html: title }} />
    {Boolean(inner.length) && (<ul>
      {inner.map((i) => <Li id={i.heading.id} title={i.heading.title} inner={i.children} />)}
    </ul>)}
  </li>)
}

export const init = () => {
  /* eslint-env browser */
  // const findAllLis = (li) => {
  //   const all = []
  //   while(li.parentElement && li.parentElement.parentElement && li.parentElement.parentElement.getAttribute('data-heading')) {
  //     li = li.parentElement.parentElement
  //     all.push(li)
  //   }
  //   return all
  // }
  const ents = [...document.querySelectorAll('div[data-section]')]

  if (ents.length) {
    const HALF=window.innerHeight / 2
    const rootMargin=-HALF+1+'px 0px 1px' // 0px ' + HALF + 'px'
    const io = new IntersectionObserver((entries) => {
      for (const { target, isIntersecting } of entries) {
        const section = target.id

        if (isIntersecting) {
          const li = document.querySelector(`[data-heading="${section}"]`)
          li.classList.add('Active')
        } else {
          const li = document.querySelector(`[data-heading="${section}"]`)
          li.classList.remove('Active')
        }
      }
    // }, { rootMargin: `-50%` }) // rootMargin: `${-window.innerHeight + 1}px`
    }, { rootMargin: rootMargin }) // rootMargin: `${-window.innerHeight + 1}px`

    for (const el of ents) {
      io.observe(el)
    }
  }
}