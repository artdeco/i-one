export default () => {
  /* eslint-env browser */
  // const findAllLis = (li) => {
  //   const all = []
  //   while(li.parentElement && li.parentElement.parentElement && li.parentElement.parentElement.getAttribute('data-heading')) {
  //     li = li.parentElement.parentElement
  //     all.push(li)
  //   }
  //   return all
  // }
  const ents = [...document.querySelectorAll('div[data-section]')]

  if (ents.length) {
    const HALF=window.innerHeight / 2
    const rootMargin=-HALF+1+'px 0px 1px' // 0px ' + HALF + 'px'
    const io = new IntersectionObserver((entries) => {
      for (const { target, isIntersecting } of entries) {
        const section = target.id

        if (isIntersecting) {
          const li = document.querySelector(`[data-heading="${section}"]`)
          li.classList.add('Active')
        } else {
          const li = document.querySelector(`[data-heading="${section}"]`)
          li.classList.remove('Active')
        }
      }
    // }, { rootMargin: `-50%` }) // rootMargin: `${-window.innerHeight + 1}px`
    }, { rootMargin: rootMargin }) // rootMargin: `${-window.innerHeight + 1}px`

    for (const el of ents) {
      io.observe(el)
    }
  }
}