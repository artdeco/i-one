/** @type {HTMLFormElement} */
const TildaForm=document.querySelector('.t678 form')

/**@type {HTMLInputElement}*/
const TildaName=TildaForm?TildaForm.querySelector('[name=Name]'):null
/**@type {HTMLInputElement}*/
const TildaPhone=TildaForm?TildaForm.querySelector('[name=Phone]'):null
/**@type {HTMLTextAreaElement}*/
const TildaComments=TildaForm?TildaForm.querySelector('[name=Comments]'):null
/**@type {HTMLButtonElement}*/
const TildaSubmit=TildaForm?TildaForm.querySelector('[type=submit]'):null

// if(TildaSubmit) {
//  document.body.appendChild(TildaSubmit)
//  TildaSubmit.style.width="0"
//  TildaSubmit.style.height="0"
//  TildaSubmit.style.position="fixed"
// }

/**@type {string} */
const cbName=TildaForm?TildaForm.dataset['successCallback']:null
/**@type {Function}*/
const cb=cbName?window[cbName]:null

/**@type {HTMLElement}*/
const FormSubmitteds=document.querySelectorAll('.FormSubmitted')
/**@type {HTMLElement}*/
const FormWrs=document.querySelectorAll('.FormWr')

window['SubmitTilda']=(mob)=>{
 // debugger
 /**@type {HTMLFormElement}*/
 const Form=document.querySelector(mob?'.FormMob':'.FormDesk')

 /**@type {HTMLInputElement}*/
 const Name=Form.querySelector('[name=Name]')
 /**@type {HTMLInputElement}*/
 const Phone=Form.querySelector('[name=Phone]')
 /**@type {HTMLInputElement}*/
 const Comments=Form.querySelector('[name=Comments]')

 if(!TildaForm) return false

 TildaName.value=Name.value
 TildaPhone.value=Phone.value
 TildaComments.value=Comments.value

 const newCb=function(){
  [...FormSubmitteds].forEach(FormSubmitted=>{
   FormSubmitted.style.display=''
  })

  ;[...FormWrs].forEach(FormWr=>{
   FormWr.style.opacity=0
   FormWr.style.pointerEvents='none'
  })

  cb.apply(this,arguments)
 }
 const newName=`${cbName}_patch`
 window[`${cbName}_patch`]=newCb
 TildaForm.dataset['successCallback']=newName

 TildaSubmit.click()

 return false
}