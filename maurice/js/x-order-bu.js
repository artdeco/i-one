/**
 * Play the transition animation.
 * @param {number} duration
 * @param {number} endOffset
 */
function transition(duration,endOffset) {
 let startTime
 const cb=(time)=>{
  const timeD=time-startTime
  let progress=timeD/duration
  if(progress>1) {
   window.scrollTo(0,endOffset)
   return
  }
  if(progress) {
   const current=endOffset*progress
   window.scrollTo(0,current)
  }
  requestAnimationFrame(cb)
 }
 requestAnimationFrame((time)=>{
  startTime=time
  cb(time)
 })
}

// window['guest.maurice.transition']=transition

const transitionTo=(target,shift=0)=>{
 const elRect1=target.getBoundingClientRect()
 const{top:top1}=elRect1
 let startOffset=window.pageYOffset
 const endOffset=startOffset+top1-shift
 // let scroll=current
 const duration=500
 transition(duration,endOffset)
}

// window['guest.maurice.transitionTo']=transitionTo

const spans=document.querySelectorAll('[x-order-bu]')
;[...spans].forEach((span)=>{
 const a=span.querySelector('a')
 a.onclick=(ev)=>{
  ev.preventDefault()
  return false
 }
})
;[...spans].forEach((span)=>{
 const a=span.querySelector('a')
 if(!a) return
 const href=a.href
 if(href.startsWith('#')) return
 let id
 href.replace(/#(.+$)/,(m,hash)=>{
  id=decodeURI(hash)
 })
 // const id=decodeURI(url.replace('#',''))
 const el=window[id]
 if(!el) return

 span.addEventListener('click',()=>{
  transitionTo(el,200)
 })
 // const target=a.href
})