import '@mauriceguest/app'
import {REPLS} from './repls'

/** @type {guest.maurice.MauriceConfig} */
const config = {
 statics:[
  {root:'generated/web_circuits'},
  {root:'node_modules/@webcircuits/ui/fronts',mount:'/fronts'},
  {root:'node_modules/@webcircuits/ui/backs',mount:'/backs'},
 ],
 frontendDirs: ['types', 'maurice'],
 layout: 'maurice/layout/MainLayout.html',
 output: 'maurice/docs',
 get packages() {
  // debugger
  return [
   '@mauriceguest/spectrum-icons',
   '@mauriceguest/feather-icons', // iconFramework
   '@webcircuits/ui',
  ]
  // return ['elements','circuits','`../package/luddites/elements']
 },
 replacements: [
  {
   re: /{{ company }}/g,
   replacement: '[Art Deco™](https://artd.eco)',
  },
  ...REPLS,
 ],
 pretty: false,
 HOST: process.env.HOST || 'https://artdeco.gitlab.io/i-one/',
 pages: '../pages',
 blocks: ['blocks'],
 elements: ['elements','circuits'],
 components: ['components'],
 // which prefixes to keep in the main CSS
 prefixes: ['-webkit-hyphens', '-ms-hyphens'],
 // for sitemap and social-buttons
 url: process.env.HOST || 'https://idealone.ru/cyber-cleaning',
 indexStrat:'no-ext', // html-ext, slash
 // required when pages are at org.gitlab.io/pages-name
 // local: true,
 mount: '/',
 // mount: '/docs/',
 // ajax: false,
 potracePath: '~/.maurice/potrace',
}

export default config