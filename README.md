# @ideal.one/i-one

Service of the future landing page.

A new website made with [Splendid][1]: https://artdeco.gitlab.io/i-one/.

## Copyright

(c) [Art Deco™][2] 2023

[1]: https://www.npmjs.com/package/splendid
[2]: https://artd.eco